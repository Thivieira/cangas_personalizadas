<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function usermeta()
    {
        return $this->hasMany('App\UserMeta');
    }

    public function getMetaValue($meta_key)
    {
        return $this->usermeta()->where('meta_key', $meta_key)->first()->meta_value;
    }

    public function isSuperAdmin()
    {

        $meta = $this->getMetaValue('capabilities');

        if ($meta === 'admin') {
            return true;
        } else {
            return false;
        };
    }

    public function isCustomer()
    {
        $meta = $this->getMetaValue('capabilities');

        if ($meta === 'customer') {
            return true;
        } else {
            return false;
        };
    }
}
