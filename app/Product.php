<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{

    use SoftDeletes, HasSlug;

    protected $fillable = ['name', 'details', 'price', 'description'];

    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }

    public function presentPrice()
    {
        $amount = new \NumberFormatter('pt_BR', \NumberFormatter::CURRENCY);

        return $amount->format($this->price);
    }

    public function productmeta()
    {
        return $this->hasMany('App\ProductMeta');
    }

    public function getMetaValue($meta_key)
    {
        return $this->productmeta()->where('meta_key', $meta_key)->first()->meta_value;
    }

    public function gallery()
    {
        return $this->belongsTo('App\Gallery');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Category', 'products_categories');
    }
}
