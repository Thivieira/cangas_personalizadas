<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class GalleryImage extends Model
{

    use HasSlug;

    protected $table = "gallery_images";

    protected $fillable = ['gallery_id', 'title', 'file', 'order'];

    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('slug');
    }

    public function gallery()
    {
        return $this->belongsTo('App\Gallery');
    }
}
