<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserMeta extends Model
{
    protected $table = 'usermeta';

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
