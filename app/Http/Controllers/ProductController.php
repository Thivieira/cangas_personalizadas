<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Product::with('gallery.gallery_images')->paginate();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            $product = new Product;

            $product->name = $request->name;
            $product->details = $request->details;
            $product->price = $request->price;
            $product->description = $request->description;

            $product->save();

            return $product;
        } catch (\Exception $e) {
            if ($e->errorInfo[1]) {
                $errorCode = $e->errorInfo[1];
                if ($errorCode == 1062) {
                    // houston, we have a duplicate entry problem
                    return response()->json(['error' => 'Já existe este produto cadastrado.'], 500);
                }
            }

            return response()->json($e->getMessage(), 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (is_numeric($id)) {

            $product = Product::find($id);
        } else {

            $product = Product::where('slug', $id)->get();

            if ($product->isEmpty()) {
                abort(404);
            }
        }

        $product = $product->first();

        return $product;

        // return view('admin.products.single', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (is_numeric($id)) {
            $product = Product::find($id);
        } else {
            $product = Product::where('slug', $id)->get()[0];
        }

        $product = $product->first();

        return $product;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
    }
}
