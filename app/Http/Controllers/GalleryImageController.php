<?php

namespace App\Http\Controllers;

use App\Gallery;
use App\GalleryImage;
use App\Helpers\Helper;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Intervention\Image\ImageManagerStatic as Image;

class GalleryImageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        GalleryImage::paginate()->orderBy('order', 'asc');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            if ($request->has('files')) {
                $this->insertMultipleImages($request);
            } else {
                $this->insertAnImage($request);
            }
        } catch (\Exception $e) {

            return $e;
            if ($e->errorInfo[1]) {
                $errorCode = $e->errorInfo[1];
                if ($errorCode == 1062) {
                    // houston, we have a duplicate entry problem
                    return response()->json(['error' => 'A imagem da galeria já está cadastrada.'], 500);
                }
            }

            return response()->json($e->getMessage(), 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\GalleryImage  $galleryImage
     * @return \Illuminate\Http\Response
     */
    public function show(GalleryImage $galleryImage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\GalleryImage  $galleryImage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GalleryImage $galleryImage)
    {
        $img = GalleryImage::find($galleryImage)->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\GalleryImage  $galleryImage
     * @return \Illuminate\Http\Response
     */
    public function updateOrder(Request $request)
    {
        $imgs = collect($request->images);

        $models = GalleryImage::updateOrCreate($request->images);
        // $models->update($request->images);

        // $models->each(function ($item) {
        //     $item->update(['order' => $item->order]);
        // });

        // $images = GalleryImage::whereIn('id', $request->images)->update($request->images);

        return $models->all();

        // GalleryImage::whereIn('id', $request->json())->update(['att' => 'foo']);
        // $galleryImage = GalleryImage::find($id);
        // $galleryImage->order = $request->order;
        // $galleryImage->save();
    }

    /**
     * @param Request $request
     * @return int
     */
    public function sortable(Request $request)
    {
        self::doReorder($request->input('items'), new GalleryImage);
        return 2;
    }

    public static function doReorder($items = [], $model)
    {
        if (count($items) == 0) $items = $model->all();

        $order = count($items);
        foreach ($items as $item) {
            $item = $model->find($item);
            $item->order = $order;
            $item->save();
            $order--;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\GalleryImage  $galleryImage
     * @return \Illuminate\Http\Response
     */
    public function destroy(GalleryImage $galleryImage)
    {
        $img = GalleryImage::find($galleryImage)->first();

        if ($img->file) {
            $this->imageDelete($img->gallery->slug, $img->file);
        }

        $img->delete();

        return response()->json(['message' => 'Imagem da galeria excluída']);
    }

    public function insertMultipleImages($request)
    {
        $images = $request->files;

        $allRecords = [];

        $galleryImage = new GalleryImage;

        $gallery = Gallery::find($request->galleryId);

        foreach ($images as $image) {

            $galleryImage->gallery_id = $request->galleryId;

            $galleryImage->order = $request->order;

            $name = $image->getClientOriginalName();

            $galleryImage->title = $name;

            Helper::createDirectory(storage_path('app/galleries/' . $gallery->slug));

            // $image->store('galleries/' . $gallery->slug);
            Image::make($image->getRealPath())->save(storage_path('app/galleries/' . $gallery->slug . '/' . $name));

            Helper::conversion('leaderboards', $gallery->slug, $image, [795, 300], $name);
            // Helper::createDirectory(storage_path('app/galleries/' . $gallery->slug . '/conversions/leaderboards'));
            // $leaderboard = Image::make($image->getRealPath())->resize(795, 300);
            // $leaderboard->save(storage_path('app/galleries/' . $gallery->slug . '/conversions/leaderboards/' . $name));

            Helper::conversion('thumbnails', $gallery->slug, $image, [128, 128], $name);
            // Helper::createDirectory(storage_path('app/galleries/' . $gallery->slug . '/conversions/thumbnails'));
            // $thumbnail = Image::make($image->getRealPath())->resize(320, 240);
            // $thumbnail->save(storage_path('app/galleries/' . $gallery->slug . '/conversions/thumbnails/' . $name));

            $galleryImage->file = $name;

            $allRecords[] = $galleryImage->attributesToArray();
        }

        $galleryImage::insert($allRecords);
    }

    public function insertAnImage($request)
    {
        $image = $request->file('file');

        $galleryImage = new GalleryImage;

        $gallery = Gallery::find($request->galleryId);

        $galleryImage->gallery_id = $request->galleryId;

        $galleryImage->order = $request->order;

        $galleryImage->title = $request->title;

        $name = $image->getClientOriginalName();

        Helper::createDirectory(storage_path('app/galleries/' . $gallery->slug));

        // $image->store('galleries/' . $gallery->slug);
        Image::make($image->getRealPath())->save(storage_path('app/galleries/' . $gallery->slug . '/' . $name));

        Helper::conversion('leaderboards', $gallery->slug, $image, [795, 300], $name);
        // Helper::createDirectory(storage_path('app/galleries/' . $gallery->slug . '/conversions/leaderboards'));
        // $leaderboard = Image::make($image->getRealPath())->resize(795, 300);
        // $leaderboard->save(storage_path('app/galleries/' . $gallery->slug . '/conversions/leaderboards/' . $name));

        Helper::conversion('thumbnails', $gallery->slug, $image, [320, 240], $name);
        // Helper::createDirectory(storage_path('app/galleries/' . $gallery->slug . '/conversions/thumbnails'));
        // $thumbnail = Image::make($image->getRealPath())->resize(320, 240);
        // $thumbnail->save(storage_path('app/galleries/' . $gallery->slug . '/conversions/thumbnails/' . $name));

        $galleryImage->file = $name;

        $galleryImage->save();
    }

    public function imageDelete($gallerySlug, $fileName)
    {

        $image_path = storage_path("app/galleries/$gallerySlug/$fileName");

        if (File::exists($image_path)) {
            // File::delete($image_path);
            Helper::deleteDirectory(storage_path("app/galleries/$gallerySlug"));
        }
    }

    public function serveImages(Request $request)
    {
        $this->validate($request, [
            'filename' => 'bail|required',
            'type' => 'required',
        ]);

        $image = GalleryImage::where('file', $request->filename)->with('gallery')->firstOrFail();

        switch ($request->type) {
            case 'original':
                $path = storage_path('app/galleries/' . $image->gallery->slug . '/' . $image->file);
                break;
            case 'thumbnail':
                $path = storage_path('app/galleries/' . $image->gallery->slug . '/conversions/thumbnails/' . $image->file);
                break;
            case 'leaderboard':
                $path = storage_path('app/galleries/' . $image->gallery->slug . '/conversions/leaderboards/' . $image->file);
                break;
            default:
                $path = storage_path('app/galleries/' . $image->gallery->slug . '/' . $image->file);
        }

        if (!File::exists($path)) {
            abort(404);
        }

        $file = File::get($path);

        $type = File::mimeType($path);

        $response = Response::make($file, 200);

        $response->header("Content-Type", $type);

        return $response;
    }
}
