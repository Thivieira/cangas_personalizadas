<?php

namespace App\Http\Controllers;

use App\Gallery;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\{Storage, File};
use Illuminate\Support\Str;

class GalleryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Gallery::with('gallery_images')->paginate();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            $gallery = new Gallery;

            $gallery->title = $request->title;

            $gallery->slug = Str::slug($gallery->title, '-', 'pt-BR');

            if ($request->file('image')) {

                Helper::createDirectory(storage_path('app/galleries/' . $gallery->slug));

                $image = $request->file('image');

                $image->store('galleries');

                $name = $image->getClientOriginalName();

                $extension = $image->extension();

                $gallery->image = $name . $extension;
            }


            $gallery->save();

            return $gallery;
        } catch (\Exception $e) {
            if ($e->errorInfo[1]) {
                $errorCode = $e->errorInfo[1];
                if ($errorCode == 1062) {
                    // houston, we have a duplicate entry problem
                    return response()->json(['error' => 'Já existe esta galeria cadastrada.'], 500);
                }
            }

            return response()->json($e->getMessage(), 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        if (is_numeric($id)) {

            $gallery = Gallery::findOrFail($id)->with('gallery_images');
        } else {

            $gallery = Gallery::where('slug', $id)->with('gallery_images')->get();

            if ($gallery->isEmpty()) {
                abort(404);
            }
        }

        $gallery = $gallery->first();

        return $gallery;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Gallery $gallery)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function destroy(Gallery $gallery)
    {
        $galeria = Gallery::findOrFail($gallery)->first();

        Helper::deleteDirectory(storage_path('app/galleries/' . $galeria->slug));

        $galeria->delete();

        return response()->json(['message' => 'Galeria Excluída']);
    }
}
