<?php

namespace App\Http\Controllers;

use App\ProductMeta;
use Illuminate\Http\Request;

class ProductMetaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProductMeta  $productMeta
     * @return \Illuminate\Http\Response
     */
    public function show(ProductMeta $productMeta)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductMeta  $productMeta
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductMeta $productMeta)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductMeta  $productMeta
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductMeta $productMeta)
    {
        //
    }
}
