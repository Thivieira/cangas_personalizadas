<?php

namespace App\Helpers;

use Illuminate\Support\Facades\File;
use Intervention\Image\ImageManagerStatic as Image;

class Helper
{
    public static function createDirectory($directoryPath)
    {
        if (!File::isDirectory($directoryPath)) {
            File::makeDirectory($directoryPath, 0777, true, true);
        }
    }

    public static function deleteDirectory($directoryPath)
    {
        if (File::isDirectory($directoryPath)) {
            File::deleteDirectory($directoryPath);
        }
    }

    public static function conversion($conversionName, $folderSlug, $image, $imageSize, $fileName)
    {
        $width = $imageSize[0];
        $height = $imageSize[1];
        self::createDirectory(storage_path("app/galleries/$folderSlug/conversions/$conversionName"));
        $conversion = Image::make($image->getRealPath())->resize($width, $height);
        $conversion->save(storage_path("app/galleries/$folderSlug/conversions/$conversionName/" . $fileName));
    }
}
