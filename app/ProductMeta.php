<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductMeta extends Model
{
    protected $table = 'productmeta';

    public function product()
    {
        return $this->belongsTo('App\Product');
    }
}
