const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.copyDirectory('resources/vendors', 'public/vendors').version();
mix.js('resources/js/admin/app.js', 'public/js/admin/app.js').version();
mix.sass('resources/sass/admin.scss', 'public/css/admin').version();
