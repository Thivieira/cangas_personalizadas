<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Product::class, function (Faker $faker) {
    $faker->addProvider(new \Bezhanov\Faker\Provider\Commerce($faker));

    $productName = $faker->productName;

    return [
        'name' => $productName,
        'slug' => Str::slug($productName, '-', 'pt-BR'),
        'details' => $faker->text(10),
        'price' => $faker->randomNumber(2),
        'description' => $faker->text(),
        'status' => 'public'
    ];
});
