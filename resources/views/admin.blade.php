<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Aloha Webstore</title>
    <link rel="stylesheet" href="/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="/vendors/iconfonts/ionicons/css/ionicons.css">
    <link rel="stylesheet" href="/vendors/iconfonts/typicons/src/font/typicons.css">
    <link rel="stylesheet" href="/vendors/iconfonts/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="/vendors/css/vendor.bundle.base.css">
    <link rel="stylesheet" href="/vendors/css/vendor.bundle.addons.css">
    <script src="https://kit.fontawesome.com/2e4bf8b29b.js" crossorigin="anonymous"></script>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/admin/admin.css') }}">
    <link rel="shortcut icon" href="../assets/images/favicon.png" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>

<body>
    <div id="app"></div>
    <script type="text/javascript">
        var APP_URL = {!! json_encode(url('/')) !!}
    </script>
    <script>
        var API_TOKEN = {!! json_encode(Auth::user()->api_token) !!}
        var USER = {!! json_encode(Auth::user()) !!}
    </script>
    <script src="{{ mix('js/admin/app.js') }}" defer></script>
</body>

</html>