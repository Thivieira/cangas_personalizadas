@extends('layouts.admin')

@section('content')
<div class="content-wrapper" id="create">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <h1>Criando um produto</h1>
                    </div>
                    <div class="card-body">
                        <form action="{{route('products.store')}}" method="POST">
                            @csrf
                            <div class="form-group">
                                <label for="exampleInputName1">Nome</label>
                                <input type="text" class="form-control" id="exampleInputName1" value="{{ old('name') }}"
                                    placeholder="Nome">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword4">Sumario</label>
                                <input type="text" class="form-control" id="exampleInputPassword4"
                                    value="{{ old('details') }}" placeholder="Password">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputCity1">Preço</label>
                                <input type="text" class="form-control" id="exampleInputCity1"
                                    value="{{ old('price') }}" placeholder="Location">
                            </div>
                            <div class="form-group">
                                <label for="exampleTextarea1">Descrição</label>
                                <textarea class="form-control" id="exampleTextarea1"
                                    rows="2">{{ old('description') }}</textarea>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col">

                                        <button type="button" class="btn btn-primary" data-toggle="modal"
                                            data-target="#image-modal">
                                            Enviar fotos
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-success mr-2">Salvar</button>
                            <button class="btn btn-light">Cancelar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="image-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <image-upload-modal />
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{mix('js/admin/ImageUploadModal.js')}}"></script>
@endsection