@extends('layouts.admin')

@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Produtos</h4>
                        <table class="table table-responsive table-hover">
                        <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Subtitulo</th>
                                <th>Preço</th>
                                <th>Descrição</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($products as $product)
                                <tr>
                                                              
                                    <td>{{$product->name}}</td>
                                    <td>{{$product->details}}</td>
                                    <td>{{$product->presentPrice()}}</td>
                                    <td>
                                        {{$product->description}}
                                    </td>
                                    <td><a href="{{route('admin.products.single',$product->slug)}}">Abrir</a></td>
                                
                                </tr>
                            @endforeach
                        </tbody>
                        </table>
                        {{ $products->links() }}
                    </div>
                </div>
        </div>
    </div>
</div>
@endsection