@extends('layouts.admin')

@section('content')
<div class="content-wrapper">
  <div class="row">
    <div class="col">
      <div class="card">
        <div class="card-header">
          <h1>{{$product->name}} - <a href="{{route('admin.products.edit',$product->id)}}"> <i
                class="fas fa-pencil-alt"></i> Editar</a> </h1>
        </div>
        <div class="card-body">

        </div>
      </div>
    </div>
  </div>
</div>
@endsection