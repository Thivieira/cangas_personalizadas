const state = {
    collapsed: false
};

const mutations = {
    collapsed(state) {
        state.collapsed = !state.collapsed;
    },
    sider(state, val) {
        state.collapsed = val;
    }
};

export default {
    namespaced: true,
    state,
    mutations
};
