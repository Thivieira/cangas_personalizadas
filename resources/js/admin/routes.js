import Dashboard from './views/Dashboard';
import Profile from './views/Profile';
import Categories from './views/categories/index';
import Category from './views/categories/single';
import CategoryCreate from './views/categories/create';
import CategoryEdit from './views/categories/edit';
import Products from './views/products/index';
import Product from './views/products/single';
import ProductCreate from './views/products/create';
import ProductEdit from './views/products/edit';
import Galleries from './views/galleries/index';
import Gallery from './views/galleries/single';
import GalleryCreate from './views/galleries/create';
import GalleryEdit from './views/galleries/edit';
import NotFound from './views/NotFound';

export default [
    { path: '/', name: 'index', component: Dashboard },
    { path: '/perfil', name: 'profile', component: Profile },
    { path: '/categorias', name: 'categories.index', component: Categories },
    { path: '/categorias/criar', name: 'categories.create', component: CategoryCreate },
    { path: '/categoria/:id', name: 'categories.single', component: Category },
    { path: '/categoria/:id/editar', name: 'categories.edit', component: CategoryEdit },
    { path: '/produtos', name: 'products.index', component: Products },
    { path: '/produtos/criar', name: 'products.create', component: ProductCreate },
    { path: '/produto/:id', name: 'products.single', component: Product },
    { path: '/produto/:id/editar', name: 'products.edit', component: ProductEdit },
    { path: '/galerias', name: 'galleries.index', component: Galleries },
    { path: '/galerias/criar', name: 'galleries.create', component: GalleryCreate },
    { path: '/galeria/:id', name: 'galleries.single', component: Gallery },
    { path: '/galeria/:id/editar', name: 'galleries.edit', component: GalleryEdit },
    { path: '*', component: NotFound }
]