<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resource('categories', 'CategoryController');
Route::resource('products', 'ProductController');
Route::resource('galleries', 'GalleryController');
Route::get('gallery_images/serve', 'GalleryImageController@serveImages');
Route::resource('gallery_images', 'GalleryImageController');
Route::post('gallery_images/order', 'GalleryImageController@sortable');
