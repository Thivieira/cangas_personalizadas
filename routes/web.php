<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Route::get('/admin', 'AdminController@index')->name('admin.index');

Route::any('/admin/{all}', function () {
    if (!Auth::user()) {
        return redirect('/login');
    }
    return view('admin'); //SPA app
})->where(['all' => '.*']);
// Route::get('/admin/perfil', 'AdminController@profile')->name('admin.profile');
// Route::resource('products', 'ProductController');
// Route::get('/admin/produtos', 'ProductController@index')->name('admin.products.index');
// Route::get('/admin/produtos/criar', 'ProductController@create')->name('admin.products.create');
// Route::get('/admin/produtos/{id}', 'ProductController@show')->name('admin.products.single');
// Route::get('/admin/produtos/{id}/editar', 'ProductController@edit')->name('admin.products.edit');

Auth::routes();
